package com.company;

import java.security.cert.CollectionCertStoreParameters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Item {
    private String name;
    private double weight;

    public Item(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }
}

class Category {
    private String name;
    private List<Item> items;

    public Category(String name, List<Item> items) {
        this.name = name;
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public List<Item> getItems() {
        return items;
    }
}

public class Main {
    public static void search(List<Category> categories) {
        categories
                .stream()
                .filter(a -> a.getName().startsWith("pc"))
                .forEach(a -> System.out.println(a.getItems().stream()
                        .peek(it -> System.out.println(it.getName() + ", " + it.getWeight())).mapToDouble(Item::getWeight).max().orElse(-1.0)));
    }

    public static void main(String[] args) {
	    Item a = new Item("motherboard", 0.56);
	    Item b = new Item("cpu", 0.13);
	    Item c = new Item("mouse", 0.32);
	    Item d = new Item("keyboard", 0.51);
	    Item e = new Item("tv", 6.11);
	    Item f = new Item("gpu", 0.67);
	    Item g = new Item("dvd_drive", 0.43);
	    Item h = new Item("iron", 1.39);
	    Item i = new Item("webcam", 0.25);
	    Item j = new Item("hdd", 0.34);

	    List<Category> categories = new ArrayList<>();
	    categories.add(new Category("electronics", Arrays.asList(e, h)));
	    search(categories);

	    categories.add(new Category("pc_parts", Arrays.asList(a, b, f, g, j)));
	    categories.add(new Category("pc_accessories", Arrays.asList(c, d, i)));
	    search(categories);
    }
}
